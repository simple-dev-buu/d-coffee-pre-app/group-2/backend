import { PartialType } from '@nestjs/swagger';
import { CreateSalaryDto } from './create-salary.dto';
import { IsInt, IsNumber, IsString } from 'class-validator';

export class UpdateSalaryDto extends PartialType(CreateSalaryDto) {
  @IsInt()
  id: number;

  @IsString()
  period: string;

  @IsString()
  paidDate: string;

  @IsInt()
  empId: number;

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsNumber()
  netSalary: number;

  @IsString()
  state: 'Paid' | 'Pending';
}
