import { IsInt, IsNumber, IsString } from 'class-validator';

export class CreateSalaryDto {
  @IsString()
  period: string;

  @IsString()
  paidDate: string;

  @IsInt()
  empId: number;

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsNumber()
  netSalary: number;

  @IsString()
  state: 'Paid' | 'Pending';
}
