import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  period: string;

  @Column()
  paidDate: string;

  @Column()
  empId: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  netSalary: number;

  @Column()
  state: 'Paid' | 'Pending';
}
