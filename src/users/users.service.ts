import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    // const parseRole = createUserDto.roles;
    // createUserDto.roles = parseRole[0];
    const role = await this.rolesRepository.findBy({
      name: createUserDto.roles,
    });
    const user = new User();
    user.email = createUserDto.email;
    user.name = createUserDto.name;
    user.password = createUserDto.password;
    user.roles = role;
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({
      relations: { roles: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneByOrFail({ email });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.name = updateUserDto.name;
    user.password = updateUserDto.password;
    user.roles = JSON.parse(updateUserDto.roles);
    const updateUser = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    updateUser.email = user.email;
    updateUser.name = user.name;
    updateUser.password = user.password;
    console.log(user.roles);
    console.log(updateUser.roles);
    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id == r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = user.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );
      if (index < 0) {
        updateUser.roles.splice(i, 1);
      }
    }
    await this.usersRepository.save(updateUser);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneByOrFail({ id });
    await this.usersRepository.remove(deleteUser);
    return deleteUser;
  }
}
