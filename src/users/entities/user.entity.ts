import { Replenishment } from 'src/replenishment/entities/replenishment.entity';
import { StockTaking } from 'src/stock-taking/entities/stock-taking.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from 'src/order/entities/order.entity';
import { Role } from '../../roles/entities/role.entity';
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Replenishment, (rep) => rep.user)
  replenishment: Replenishment[];

  @OneToMany(() => StockTaking, (stk) => stk.user)
  stockTakingList: StockTaking[];

  @OneToMany(() => Order, (order) => order.user)
  order: Order[];
}
