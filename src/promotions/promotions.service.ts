import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private readonly promotionRepo: Repository<Promotion>,
  ) {}

  create(createPromotionDto: CreatePromotionDto) {
    return this.promotionRepo.save(createPromotionDto);
  }

  findAll() {
    return this.promotionRepo.find();
  }

  findOne(id: number) {
    return this.promotionRepo.findOneBy({ id });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    return this.promotionRepo.update(id, updatePromotionDto);
  }

  async remove(id: number) {
    return this.promotionRepo.delete(id);
  }
}
