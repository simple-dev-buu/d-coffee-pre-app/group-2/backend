import { IsNumber, IsString } from 'class-validator';

export class CreatePromotionDto {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsNumber()
  discount: number;

  @IsString()
  startDate: string;

  @IsString()
  endDate: string;

  @IsString()
  status: 'active' | 'inactive';
}
