import { PartialType } from '@nestjs/swagger';
import { CreatePromotionDto } from './create-promotion.dto';
import { IsInt, IsNumber, IsString } from 'class-validator';

export class UpdatePromotionDto extends PartialType(CreatePromotionDto) {
  @IsInt()
  id: number;

  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsNumber()
  discount: number;

  @IsString()
  startDate: string;

  @IsString()
  endDate: string;

  @IsString()
  status: 'active' | 'inactive';
}
