import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InventoryModule } from './inventory/inventory.module';
import { BranchModule } from './branch/branch.module';
import { InventoryItemsModule } from './inventory-items/inventory-items.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmployeesModule } from './employees/employees.module';
import { PromotionsModule } from './promotions/promotions.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { SalariesModule } from './salaries/salaries.module';
import { CustomersModule } from './customers/customers.module';
import { BillsModule } from './bills/bills.module';
import { ReplenishmentModule } from './replenishment/replenishment.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { StockTakingModule } from './stock-taking/stock-taking.module';
import { StockTakingItemsModule } from './stock-taking-items/stock-taking-items.module';
import { DataSource } from 'typeorm';
import { RolesModule } from './roles/roles.module';
import { ProductsModule } from './product/products.module';
import { OrdersModule } from './order/orders.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'd-coffee.sqlite',
      synchronize: true,
      autoLoadEntities: true,
      entities: ['dist/**/*.entity{.ts,.js}'],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    AuthModule,
    InventoryModule,
    BranchModule,
    InventoryItemsModule,
    IngredientsModule,
    EmployeesModule,
    PromotionsModule,
    UsersModule,
    SalariesModule,
    CustomersModule,
    BillsModule,
    ReplenishmentModule,
    StockTakingModule,
    StockTakingItemsModule,
    RolesModule,
    ProductsModule,
    OrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
