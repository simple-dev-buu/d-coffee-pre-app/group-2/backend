import { Exclude } from 'class-transformer';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { StockTakingItem } from 'src/stock-taking-items/entities/stock-taking-item.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class StockTaking {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: new Date().toISOString() })
  dateCreated: string;

  @OneToMany(() => StockTakingItem, (stk) => stk.stockTaking, {
    cascade: ['insert', 'remove'],
  })
  stockTakingItems: StockTakingItem[];

  @ManyToOne(() => User, (user) => user.stockTakingList)
  @JoinColumn()
  @Exclude()
  user: User;

  @ManyToOne(() => Inventory, (inv) => inv.stockTakingList)
  @JoinColumn()
  inventory: Inventory;
}
