import { Module } from '@nestjs/common';
import { StockTakingService } from './stock-taking.service';
import { StockTakingController } from './stock-taking.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockTaking } from './entities/stock-taking.entity';
import { StockTakingItem } from 'src/stock-taking-items/entities/stock-taking-item.entity';
import { User } from 'src/users/entities/user.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { InventoryItem } from 'src/inventory-items/entities/inventory-item.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StockTaking,
      StockTakingItem,
      User,
      Inventory,
      InventoryItem,
    ]),
  ],
  controllers: [StockTakingController],
  providers: [StockTakingService],
})
export class StockTakingModule {}
