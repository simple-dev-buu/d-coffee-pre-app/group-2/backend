import { IsArray, IsNumber, IsString } from 'class-validator';
import { CreateStockTakingItemDto } from 'src/stock-taking-items/dto/create-stock-taking-item.dto';

export class CreateStockTakingDto {
  @IsString()
  dateCreated: string;

  @IsArray()
  stockTakingItems: CreateStockTakingItemDto[];

  @IsNumber()
  userId: number;

  @IsNumber()
  inventoryId: number;
}
