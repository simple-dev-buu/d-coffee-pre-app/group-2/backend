import { Injectable } from '@nestjs/common';
import { CreateStockTakingDto } from './dto/create-stock-taking.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { StockTaking } from './entities/stock-taking.entity';
import { Repository } from 'typeorm';
import { StockTakingItem } from 'src/stock-taking-items/entities/stock-taking-item.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { User } from 'src/users/entities/user.entity';
import { InventoryItem } from 'src/inventory-items/entities/inventory-item.entity';

@Injectable()
export class StockTakingService {
  constructor(
    @InjectRepository(StockTaking)
    private readonly repo: Repository<StockTaking>,
    @InjectRepository(StockTakingItem)
    private readonly repoStockItems: Repository<StockTakingItem>,
    @InjectRepository(Inventory)
    private readonly repoInventory: Repository<Inventory>,
    @InjectRepository(InventoryItem)
    private readonly repoInventoryItem: Repository<InventoryItem>,
    @InjectRepository(User)
    private readonly repoUser: Repository<User>,
  ) {}

  async create(dto: CreateStockTakingDto) {
    const inv = await this.repoInventory.findOneByOrFail({
      id: dto.inventoryId,
    });
    const user = await this.repoUser.findOneByOrFail({ id: dto.userId });
    const stkItems = await Promise.all(
      dto.stockTakingItems.map(async (item) => {
        const invItem = await this.repoInventoryItem.findOneByOrFail({
          id: item.inventoryItemId,
        });
        const stkItem = this.repoStockItems.create({
          inventoryItem: invItem,
          balanceOld: item.balanceOld,
          balanceUpdate: item.balanceUpdate,
        });
        return stkItem;
      }),
    );
    const stock = this.repo.create({
      inventory: inv,
      dateCreated: dto.dateCreated,
      stockTakingItems: stkItems,
      user: user,
    });
    return this.repo.save(stock);
  }

  findAll() {
    return this.repo.find({
      relations: {
        user: true,
        inventory: { branch: true },
      },
    });
  }

  findOne(id: number) {
    return this.repo.findOne({
      where: { id: id },
      relations: {
        user: true,
        inventory: { branch: true },
        stockTakingItems: true,
      },
    });
  }

  remove(id: number) {
    return this.repo.delete(id);
  }
}
