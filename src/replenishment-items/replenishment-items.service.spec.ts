import { Test, TestingModule } from '@nestjs/testing';
import { ReplenishmentItemsService } from './replenishment-items.service';

describe('ReplenishmentItemsService', () => {
  let service: ReplenishmentItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReplenishmentItemsService],
    }).compile();

    service = module.get<ReplenishmentItemsService>(ReplenishmentItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
