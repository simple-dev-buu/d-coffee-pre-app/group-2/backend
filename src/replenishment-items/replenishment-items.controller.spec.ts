import { Test, TestingModule } from '@nestjs/testing';
import { ReplenishmentItemsController } from './replenishment-items.controller';
import { ReplenishmentItemsService } from './replenishment-items.service';

describe('ReplenishmentItemsController', () => {
  let controller: ReplenishmentItemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReplenishmentItemsController],
      providers: [ReplenishmentItemsService],
    }).compile();

    controller = module.get<ReplenishmentItemsController>(
      ReplenishmentItemsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
