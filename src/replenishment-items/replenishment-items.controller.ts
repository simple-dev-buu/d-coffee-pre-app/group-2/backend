import { Controller } from '@nestjs/common';
import { ReplenishmentItemsService } from './replenishment-items.service';

@Controller('replenishment-items')
export class ReplenishmentItemsController {
  constructor(
    private readonly replenishmentItemsService: ReplenishmentItemsService,
  ) {}
}
