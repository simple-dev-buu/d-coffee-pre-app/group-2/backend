import { Module } from '@nestjs/common';
import { ReplenishmentItemsService } from './replenishment-items.service';
import { ReplenishmentItemsController } from './replenishment-items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReplenishmentItem } from './entities/replenishment-items.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ReplenishmentItem, Ingredient])],
  controllers: [ReplenishmentItemsController],
  providers: [ReplenishmentItemsService],
  exports: [ReplenishmentItemsService],
})
export class ReplenishmentItemsModule {}
