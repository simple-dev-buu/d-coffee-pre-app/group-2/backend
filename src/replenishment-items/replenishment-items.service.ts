import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReplenishmentItem } from './entities/replenishment-items.entity';
import { Repository } from 'typeorm';
// import { ReplenishmentItemDto } from './dto/create-replenishment-items.dto';
// import { InventoryItemsService } from 'src/inventory-items/inventory-items.service';

@Injectable()
export class ReplenishmentItemsService {
  constructor(
    @InjectRepository(ReplenishmentItem)
    private readonly repo: Repository<ReplenishmentItem>,
  ) {}
  // async create(dto: ReplenishmentItemDto) {
  //   const invItm = await this.invItemService.findOne(dto.inventoryItemId);
  //   const item = this.repo.create({
  //     amountAdd: dto.amountAdd,
  //     amountOld: dto.amountOld,
  //     netValue: dto.netValue,
  //     inventoryItem: invItm,
  //   });
  //   return item;
  // }
}
