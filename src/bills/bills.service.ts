import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private BillRepo: Repository<Bill>,
  ) {}

  create(createBillDto: CreateBillDto) {
    const bill = new Bill();
    bill.name = createBillDto.name;
    bill.worth = createBillDto.worth;
    bill.status = createBillDto.status;
    return this.BillRepo.save(bill);
  }

  findAll() {
    return this.BillRepo.find();
  }

  findOne(id: number) {
    return this.BillRepo.findOneBy({ id: id });
  }

  findByStatus(status: boolean) {
    return this.BillRepo.find({
      where: {
        status: status,
      },
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.BillRepo.update(id, updateBillDto);
    const bill = await this.BillRepo.findOneBy({ id: id });
    return bill;
  }

  async remove(id: number) {
    const delBill = await this.BillRepo.findOneByOrFail({ id: id });
    await this.BillRepo.remove(delBill);
    return delBill;
  }
}
