import { PartialType } from '@nestjs/swagger';
import { CreateBillDto } from './create-bill.dto';
import { IsBoolean, IsInt, IsNumber, IsString } from 'class-validator';

export class UpdateBillDto extends PartialType(CreateBillDto) {
  @IsInt()
  id: number;

  @IsString()
  name: string;

  //@IsDate()
  //createDate: Date;

  //@IsString()
  //updateDate: Date;

  @IsNumber()
  worth: number;

  @IsBoolean()
  status: boolean;
}
