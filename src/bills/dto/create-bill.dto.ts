import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class CreateBillDto {
  @IsString()
  name: string;
  //@IsDate()
  //createDate: Date;
  //@IsString()
  //updateDate: Date;
  @IsNumber()
  worth: number;
  @IsBoolean()
  status: boolean;
}
