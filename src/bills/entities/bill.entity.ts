import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @CreateDateColumn()
  createDate: Date;
  @UpdateDateColumn()
  updateDate: Date;
  @Column()
  worth: number;
  @Column({ default: false })
  status: boolean;
  //@ManyToOne(() => Branch, (branch) => branch.bill)
  //branch: Branch;
}
