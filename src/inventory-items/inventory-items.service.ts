import { Injectable } from '@nestjs/common';
import { CreateInventoryItemDto } from './dto/create-inventory-item.dto';
import { UpdateInventoryItemDto } from './dto/update-inventory-item.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { InventoryItem } from './entities/inventory-item.entity';
import { Repository } from 'typeorm';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';

@Injectable()
export class InventoryItemsService {
  constructor(
    @InjectRepository(InventoryItem)
    private readonly repo: Repository<InventoryItem>,
    @InjectRepository(Inventory)
    private readonly repoInventory: Repository<Inventory>,
    @InjectRepository(Ingredient)
    private readonly repoIng: Repository<Ingredient>,
  ) {}

  async create(dto: CreateInventoryItemDto) {
    const ing = await this.repoIng.findOneBy({ id: dto.ingredientId });
    const item = this.repo.create({
      balance: dto.balance,
      minBalance: dto.minBalance,
      value: dto.value,
      ingredient: ing,
    });
    return item;
  }

  findAll() {
    return this.repo.find();
  }

  findOne(id: number) {
    return this.repo.findOne({
      where: { id: id },
      relations: { ingredient: true },
    });
  }

  async update(id: number, dto: UpdateInventoryItemDto) {
    const ing = await this.repoIng.findOneBy({ id: dto.ingredientId });
    const inventory = await this.repoInventory.findOneByOrFail({ id: id });
    const item = this.repo.create({
      id: dto.id,
      balance: dto.balance,
      minBalance: dto.minBalance,
      value: dto.value,
      ingredient: ing,
      inventory: inventory,
    });
    // return this.repo.save(item);
    return item;
  }

  remove(id: number) {
    return this.repo.delete(id);
  }
}
