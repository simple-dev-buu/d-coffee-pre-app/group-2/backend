import { PartialType } from '@nestjs/swagger';
import { CreateInventoryItemDto } from './create-inventory-item.dto';
import { IsInt, IsNumber } from 'class-validator';

export class UpdateInventoryItemDto extends PartialType(
  CreateInventoryItemDto,
) {
  @IsInt()
  id: number;

  @IsInt()
  inventoryId: number;

  @IsInt()
  ingredientId: number;

  @IsNumber()
  balance: number;

  @IsNumber()
  minBalance: number;

  @IsNumber()
  value: number;
}
