import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StockTakingItem } from './entities/stock-taking-item.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StockTakingItemsService {
  constructor(
    @InjectRepository(StockTakingItem)
    private readonly repo: Repository<StockTakingItem>,
  ) {}

  // create(dto: CreateStockTakingItemDto) {
  //   return '';
  // }

  findAll() {
    return this.repo.find({ relations: { stockTaking: true } });
  }

  findOne(id: number) {
    return this.repo.findOneBy({ id });
  }

  remove(id: number) {
    return this.repo.delete(id);
  }
}
