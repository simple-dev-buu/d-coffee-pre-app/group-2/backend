import { Module } from '@nestjs/common';
import { StockTakingItemsService } from './stock-taking-items.service';
import { StockTakingItemsController } from './stock-taking-items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockTakingItem } from './entities/stock-taking-item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockTakingItem])],
  controllers: [StockTakingItemsController],
  providers: [StockTakingItemsService],
})
export class StockTakingItemsModule {}
