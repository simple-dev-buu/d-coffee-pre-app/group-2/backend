import { Test, TestingModule } from '@nestjs/testing';
import { StockTakingItemsService } from './stock-taking-items.service';

describe('StockTakingItemsService', () => {
  let service: StockTakingItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StockTakingItemsService],
    }).compile();

    service = module.get<StockTakingItemsService>(StockTakingItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
