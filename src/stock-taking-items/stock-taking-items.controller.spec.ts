import { Test, TestingModule } from '@nestjs/testing';
import { StockTakingItemsController } from './stock-taking-items.controller';
import { StockTakingItemsService } from './stock-taking-items.service';

describe('StockTakingItemsController', () => {
  let controller: StockTakingItemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockTakingItemsController],
      providers: [StockTakingItemsService],
    }).compile();

    controller = module.get<StockTakingItemsController>(
      StockTakingItemsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
