import { Controller, Get, Param, Delete } from '@nestjs/common';
import { StockTakingItemsService } from './stock-taking-items.service';

@Controller('stock-taking-items')
export class StockTakingItemsController {
  constructor(
    private readonly stockTakingItemsService: StockTakingItemsService,
  ) {}

  @Get()
  findAll() {
    return this.stockTakingItemsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stockTakingItemsService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stockTakingItemsService.remove(+id);
  }
}
