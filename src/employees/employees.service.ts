import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private readonly employeeRepo: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    return this.employeeRepo.save(createEmployeeDto);
  }

  findAll(): Promise<Employee[]> {
    return this.employeeRepo.find();
  }

  findOne(id: number) {
    return this.employeeRepo.findOneBy({ id: id });
  }

  findOneByTelephone(tel: string) {
    return this.employeeRepo.findOneByOrFail({ tel });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    await this.employeeRepo.update(id, updateEmployeeDto);
    const employee = await this.employeeRepo.findOneBy({ id });
    return employee;
  }

  async remove(id: number) {
    const deleteEmployee = await this.employeeRepo.findOneBy({ id });
    return this.employeeRepo.remove(deleteEmployee);
  }
}
