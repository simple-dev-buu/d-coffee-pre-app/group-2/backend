import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import { Order } from 'src/order/entities/order.entity';
@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  tel: string;

  @Column({ default: 0 })
  point: number;

  @Column()
  birthDate: string;

  @CreateDateColumn()
  registerDate: string;

  @OneToMany(() => Order, (order) => order.customer)
  order: Order[];
}
