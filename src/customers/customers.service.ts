import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private readonly customerRepo: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto): Promise<Customer> {
    return this.customerRepo.save(createCustomerDto);
  }

  findAll(): Promise<Customer[]> {
    return this.customerRepo.find();
  }

  findOne(id: number) {
    return this.customerRepo.findOneBy({ id });
  }

  findOneByTelephone(tel: string) {
    return this.customerRepo.findOneByOrFail({ tel });
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    await this.customerRepo.update(id, updateCustomerDto);
    const customer = await this.customerRepo.findOneBy({ id });
    return customer;
  }

  async remove(id: number) {
    const deleteCustomer = await this.customerRepo.findOneBy({ id });
    return this.customerRepo.remove(deleteCustomer);
  }
}
