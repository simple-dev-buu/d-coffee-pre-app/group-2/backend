import { IsArray, IsInt, IsNumber, IsString } from 'class-validator';
import { CreateReplenishmentItemDto } from 'src/replenishment-items/dto/create-replenishment-items.dto';

export class CreateReplenishmentDto {
  @IsString()
  createdDate: string;

  @IsInt()
  userId: number;

  @IsNumber()
  totalCost: number;

  @IsInt()
  inventoryId: number;

  @IsArray()
  replenishmentItems: CreateReplenishmentItemDto[];
}
