import { Injectable } from '@nestjs/common';
import { CreateReplenishmentDto } from './dto/create-replenishment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Replenishment } from './entities/replenishment.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { ReplenishmentItem } from 'src/replenishment-items/entities/replenishment-items.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';

@Injectable()
export class ReplenishmentService {
  constructor(
    @InjectRepository(Replenishment)
    private readonly repo: Repository<Replenishment>,
    @InjectRepository(ReplenishmentItem)
    private readonly repoItems: Repository<ReplenishmentItem>,
    @InjectRepository(Inventory)
    private readonly repoInventory: Repository<Inventory>,
    @InjectRepository(User)
    private readonly repoUser: Repository<User>,
    @InjectRepository(Ingredient)
    private readonly repoIng: Repository<Ingredient>,
  ) {}

  async create(dto: CreateReplenishmentDto) {
    const user = await this.repoUser.findOneByOrFail({ id: dto.userId });
    const inv = await this.repoInventory.findOneByOrFail({
      id: dto.inventoryId,
    });
    const items = await Promise.all(
      dto.replenishmentItems.map(async (item) => {
        const ing = await this.repoIng.findOneByOrFail({
          id: item.ingredientId,
        });
        return this.repoItems.create({ ...item, ingredient: ing });
      }),
    );
    const replenishment = this.repo.create({
      createdDate: dto.createdDate,
      user: user,
      totalCost: dto.totalCost,
      inventory: inv,
      replenishmentItems: items,
    });
    return this.repo.save(replenishment);
  }

  findAll() {
    return this.repo.find({
      relations: {
        inventory: { branch: true },
        user: true,
      },
    });
  }

  findOne(id: number) {
    return this.repo.findOneBy({ id });
  }
  findOneById(id: number) {
    return this.repo.find({
      where: { id: id },
      relations: {
        inventory: { branch: true },
        replenishmentItems: { ingredient: true },
        user: true,
      },
    });
  }

  remove(id: number) {
    return this.repo.delete({ id });
  }
}
