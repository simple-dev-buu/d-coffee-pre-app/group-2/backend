import { Exclude } from 'class-transformer';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { ReplenishmentItem } from 'src/replenishment-items/entities/replenishment-items.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Replenishment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: new Date().toISOString().slice(0, 10) })
  createdDate: string;

  @ManyToOne(() => User, (user) => user.replenishment)
  @JoinColumn()
  @Exclude({ toPlainOnly: true })
  user: User;

  @Column()
  totalCost: number;
  @OneToMany(() => ReplenishmentItem, (rep) => rep.replenishment, {
    cascade: ['insert', 'remove'],
  })
  replenishmentItems: ReplenishmentItem[];

  @ManyToOne(() => Inventory, (inv) => inv.replenishment)
  @JoinColumn()
  inventory: Inventory;
}
