import { Module } from '@nestjs/common';
import { ReplenishmentService } from './replenishment.service';
import { ReplenishmentController } from './replenishment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Replenishment } from './entities/replenishment.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { ReplenishmentItem } from 'src/replenishment-items/entities/replenishment-items.entity';
import { User } from 'src/users/entities/user.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';
import { ReplenishmentItemsModule } from 'src/replenishment-items/replenishment-items.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Replenishment,
      Inventory,
      ReplenishmentItem,
      User,
      Ingredient,
    ]),
    ReplenishmentItemsModule,
  ],
  controllers: [ReplenishmentController],
  providers: [ReplenishmentService],
})
export class ReplenishmentModule {}
