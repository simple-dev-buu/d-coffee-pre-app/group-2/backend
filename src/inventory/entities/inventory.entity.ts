import { Branch } from 'src/branch/entities/branch.entity';
import { InventoryItem } from 'src/inventory-items/entities/inventory-item.entity';
import { Replenishment } from 'src/replenishment/entities/replenishment.entity';
import { StockTaking } from 'src/stock-taking/entities/stock-taking.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Inventory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  totalValue: number;

  @OneToMany(() => Replenishment, (rep) => rep.inventory)
  replenishment: Replenishment[];

  @OneToMany(() => InventoryItem, (inv) => inv.inventory, {
    cascade: ['insert', 'remove', 'update'],
  })
  inventoryItems: InventoryItem[];

  @OneToOne(() => Branch)
  @JoinColumn()
  branch: Branch;

  @OneToMany(() => StockTaking, (stk) => stk.inventory)
  stockTakingList: StockTaking[];
}
