import { IsInt, IsNumber, IsOptional } from 'class-validator';
import { CreateInventoryItemDto } from 'src/inventory-items/dto/create-inventory-item.dto';

export class CreateInventoryDto {
  @IsInt()
  branchId: number;

  @IsNumber()
  totalValue: number;

  @IsOptional()
  inventoryItems: CreateInventoryItemDto[];
}
