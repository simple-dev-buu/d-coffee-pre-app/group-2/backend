import { PartialType } from '@nestjs/mapped-types';
import { CreateInventoryDto } from './create-inventory.dto';
import { IsInt, IsNumber, IsOptional } from 'class-validator';
import { UpdateInventoryItemDto } from 'src/inventory-items/dto/update-inventory-item.dto';

export class UpdateInventoryDto extends PartialType(CreateInventoryDto) {
  @IsInt()
  id: number;

  @IsNumber()
  branchId: number;

  @IsNumber()
  totalValue: number;

  @IsOptional()
  inventoryItems: UpdateInventoryItemDto[];
}
